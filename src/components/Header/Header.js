import './Header.scss';
import {useLocation, Link} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';

function Header() {

    const location = useLocation();
    const dispatch = useDispatch();

    const cartStore = useSelector(state => {return state.cart});

    return (
        <header className="header">
            <nav className="header__nav">
                <ul className="header__list">
                    <li className="header__item header__item--small">
                        <a href="https://www.apple.com/fr/" target="_blank" rel="noreferrer" ><img src={'apple.png'} alt="Apple Logo"/></a>
                    </li>
                    <li className={`header__item ${location.pathname === '/iphone' ? 'selected' : null}`}>
                        <Link to="/iphone">Iphone</Link>    
                    </li>
                    <li className={`header__item ${location.pathname === '/' ? 'selected' : null}`}>
                        <Link to="/">Airpods Max</Link>
                    </li>
                    <li className="header__item header__item--small">
                        <div className='header__cart' onClick={() => {
                            dispatch({
                                type: "cart/toggleShowCart",
                                payload: !cartStore.showCart,
                            })
                        }}><img src={'cart.png'} alt="Cart Logo"/>
                            {cartStore.cartList.length > 0 && <div className='header__number'>
                                {cartStore.cartList.length}
                            </div>}
                        </div>
                    </li>
                </ul>
            </nav>
        </header>
    );
  }
  
  export default Header;