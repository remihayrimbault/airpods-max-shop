import Header from '../Header/Header';
import Cart from '../Cart/Cart';

function Layout(props) {

    return <>
        <Header></Header>
        <main>
            <Cart/>
            {props.children}
        </main>
        <footer>
            <p>La Rochelle Université</p>
        </footer>
    </>
}

export default Layout;
