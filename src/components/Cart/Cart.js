import { useDispatch, useSelector } from 'react-redux';
import './Cart.scss';
import CartItem from '../CartItem/CartItem';

function Cart () {

    const cartStore = useSelector(state => {return state.cart});

    const showCart = cartStore.showCart;

    const cartList = cartStore.cartList;

    const dispatch = useDispatch();

    return (
       <div className={`cart ${showCart ? 'isOpen' : null}`}>
            <h2 className='cart__title'>Cart</h2>
            <div className='cart__productList'>
                <p className='cart__productInfo cart__productName cart__productInfo--top'>Product</p>
                <p className='cart__productInfo cart__productColor cart__productInfo--top'>Color</p>
                <p className='cart__productInfo cart__productPrice cart__productInfo--top'>Price</p>
                {cartList.map(product => <CartItem item={product}></CartItem>)}
                {cartList.length === 0 && <p className='cart__emptyList'>Your cart is empty.</p>}
            </div>
            <button className='cart__buyButton'>
                <p>Buy</p>
            </button>
       </div>
     );
}

export default Cart;