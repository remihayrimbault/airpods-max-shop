import { useDispatch } from 'react-redux';

function CartItem (props) {

    const {
        item
    } = props

    const dispatch = useDispatch();

    const id = item.id;

    const productName = item.product === 'iphone' ? 'Iphone 13 Pro Max' : 'Airpods Max';

    const color = item.color;

    const price = item.product === 'iphone' ? '1200€' : '600€';

    return (<>
            <p className='cart__productInfo cart__productName'>{productName}</p>
            <p className='cart__productInfo cart__productColor'>{color}</p>
            <div className='cart__productInfo cart__productPrice'>
                {price}
                <div className='cart__removeProduct' onClick={() => {
                    dispatch({
                        type: "cart/deleteProduct",
                        payload: id,
                    })
                }}><img src={'x-mark.png'} alt="X Mark"/></div>
            </div>
        </>);
}

export default CartItem;