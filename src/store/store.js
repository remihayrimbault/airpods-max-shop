import { configureStore, createSlice } from "@reduxjs/toolkit";

const cart = createSlice({
    name: "cart",
    initialState: {
        cartList: [
            {id: 1, product: "iphone", color: "green"}
        ],
        showCart: false,
    },
    reducers: {
        addProduct: (state, action) => {
            const newProduct = {
                id: action.payload.id,
                product: action.payload.product,
                color: action.payload.color
            }
            state.cartList.push(newProduct);
        },
        editProduct: (state, action) => {
            let currentProduct = state.find(product => product.id === action.payload.id);
            currentProduct.product = action.payload.product;
            currentProduct.color = action.payload.color;
        },
        deleteProduct: (state, action) => {
            state.cartList = state.cartList.filter(product => product.id !== action.payload);
        },
        toggleShowCart: (state, action) => {
            state.showCart = action.payload;
        },
    }
})

export const store = configureStore({
    reducer: {
        cart: cart.reducer,
    }
})