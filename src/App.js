import Layout from './components/Layout/Layout';
import AirpodsMax from './views/AirpodsMax/AipodsMax';
import Iphone from './views/Iphone/Iphone';
import {BrowserRouter, Route, Routes} from "react-router-dom";

function App() {

  return (
    <BrowserRouter>
      <Routes>
          <Route index={true} element={
              <Layout>
                  <AirpodsMax/>
              </Layout>
          } />
          <Route path="/iphone" element={
              <Layout>
                  <Iphone/>
              </Layout>
          } />
      </Routes>
    </BrowserRouter>
  )
}

export default App;
